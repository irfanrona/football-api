<?php

namespace App\Http\Controllers\Api;

use App\Game;
use App\Club;
use App\Http\Controllers\Controller;
use App\Http\Resources\ClubCollection;
use App\Http\Resources\ClubItem;
use App\Http\Resources\GameCollection;
use App\Http\Resources\GameItem;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class GameController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return new GameCollection(Game::get());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //$this->validate($request, Game::rules(false));

        if (!Game::create($request->all())) {
            return [
                'message' => 'Bad Request',
                'code' => 400,
            ];
        } else {
            $home = (int)Str::before($request->score, ':');
            $away = (int)Str::after($request->score, ':');

            if ($home > $away) {
                $homePoint = 3;
                $awayPoint = 0;
            }else if ($home < $away) {
                $homePoint = 0;
                $awayPoint = 3;
            }else{
                $homePoint = 1;
                $awayPoint = 1;
            }

            //update home club point
            $home = Club::where('club_name', $request->clubhomename)->first();
            $home->points += $homePoint;
            if (!$home->save()) {
                return [
                    'message' => 'Bad Request',
                    'code' => 400,
                ];
            }

            //update away club point
            $away = Club::where('club_name', $request->clubawayname)->first();
            $away->points += $awayPoint;
            $away->save();
            if (!$away->save()) {
                return [
                    'message' => 'Bad Request',
                    'code' => 400,
                ];
            }

            return [
                'message' => 'OK',
                'code' => 200,
            ];
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Game  $game
     * @return \Illuminate\Http\Response
     */
    public function show(Game $game)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Game  $game
     * @return \Illuminate\Http\Response
     */
    public function edit(Game $game)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Game  $game
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Game $game)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Game  $game
     * @return \Illuminate\Http\Response
     */
    public function destroy(Game $game)
    {
        //
    }
}
