<?php

namespace App\Http\Controllers\Api;

use App\Club;
use App\Http\Controllers\Controller;
use App\Http\Resources\ClubCollection;
use App\Http\Resources\ClubItem;
use App\Http\Resources\StandingCollection;
use App\Http\Resources\StandingItem;
use Illuminate\Http\Request;

class ClubController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return new ClubCollection(Club::get());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //$this->validate($request, Club::rules(false));

        if (!Club::create($request->all())) {
            return [
                'message' => 'Bad Request',
                'code' => 400,
            ];
        } else {
            return [
                'message' => 'OK',
                'code' => 200,
            ];
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Club  $club
     * @return \Illuminate\Http\Response
     */
    public function show(Club $club)
    {
        return new ClubItem($club);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Club  $club
     * @return \Illuminate\Http\Response
     */
    public function edit(Club $club)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Club  $club
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //$this->validate($request, Club::rules(true, $club->id));
        $data = Club::find($id);
        $data->club_name = $request->input('club_name');
        $data->points = $request->input('points');

        if (!$data->save()) {
            return [
                'message' => 'Bad Request',
                'code' => 400,
            ];
        } else {
            return [
                'message' => 'OK',
                'code' => 200,
            ];
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Club  $club
     * @return \Illuminate\Http\Response
     */
    public function destroy(Club $club)
    {
        if ($club->delete()) {
            return [
                'message' => 'OK',
                'code' => 204,
            ];
        } else {
            return [
                'message' => 'Bad Request',
                'code' => 400,
            ];
        }
    }

    public function clubstanding(Request $req)
    {
        if($req->input('clubname')){

            $data = Club::orderBy('points','desc')->get();
            
            $orderedItems = $data->map(function ($item, $index) {
                $item['standing'] =  $index + 1;
                return $item;
            });

            $found = $orderedItems->where('club_name', $req->input('clubname'));

            return new StandingCollection($found);
        }
    }

    public function allstanding()
    {
        return new ClubCollection(Club::orderBy('points', 'desc')->get());
    }
}
