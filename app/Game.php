<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Validation\Rule;

class Game extends Model
{
    protected $guarded = ['id'];

    /*
     * Validations
     */

    public static function rules($update = false, $id = null)
    {
        $rules = [
            'clubhome_id '    => 'required',
            'clubaway_id '    => 'required',
            'score'         => 'required'
        ];

        return $rules;
        
    }

    public function club()
    {
        return $this->belongsTo(Club::class);
    }
}
