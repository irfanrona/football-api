<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Validation\Rule;

class Club extends Model
{
    protected $guarded = ['id'];

    /*
     * Validations
     */

    public static function rules($update = false, $id = null)
    {
        $rules = [
            'club_name'    => 'required|unique',
            'points'         => 'required'
        ];

        return $rules;
        
    }

    public function games()
    {
        return $this->hasMany(Game::class);
    }
}
