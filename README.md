<p align="center"><img src="https://res.cloudinary.com/dtfbvvkyp/image/upload/v1566331377/laravel-logolockup-cmyk-red.svg" width="400"></p>

## About Football-API

Laravel web application testing API as case study of Football League.

### Prerequisites

* [Php 7.4.6](https://www.php.net/) - PHP is a popular general-purpose scripting language
* [Composer Latest: 1.10.7](https://getcomposer.org/) - A Dependency Manager for PHP

## Built With

* [Laravel 7.x](https://laravel.com/) - The web framework used

## How to Install

1. Clone Project with HTTPS
```
    git clone https://gitlab.com/irfanrona/football-api.git
```
2. cd to repo folder
3. Installing dependencies
```
   composer install
```
4. Create a copy of environment
```
   cp .env.example .env
```
5. Generate app key
```
   php artisan key:generate
```
6. Set database and database info in .env file
7. Migrate database
```
   php artisan migrate
```

8. Seed the database

*Optional

I create dummy data for team club using faker 
```
   php artisan db:seed
```
## Run App

Run app with php in localhost with 8000 port
```
    php artisan serve
```

## How to use API

There is a Postman Collection file ```football-api.postman_collection.json``` that can be used.

- All Leauge Standing
For example, a GET: 127.0.0.1:8000/api/leaguestanding

- Club Standing
For example, a GET: 127.0.0.1:8000/api/rank?clubname=Washington

```query param
    clubname : 'Club Name'
```

### Create Match Record
For example, a POST: 127.0.0.1:8000/api/games/

```body param
    clubhomename : 'Club Home'
    clubawayname : 'Club Away'
    score : '3 : 0'
```

### All Match Record
For example, a GET: 127.0.0.1:8000/api/games/

### Create New Team Club
For example, a POST: 127.0.0.1:8000/api/clubs/

```body param
    clubhomename : 'Man Blue'
    points : 0
```

*club name value still case-sensitive

## Contributing

Thank you for considering contributing to the Laravel framework! The contribution guide can be found in the [Laravel documentation](https://laravel.com/docs/contributions).

## License

The Laravel framework is open-sourced software licensed under the [MIT license](https://opensource.org/licenses/MIT).
