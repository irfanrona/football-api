<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use Faker\Generator as Faker;

$factory->define(\App\Club::class, function (Faker $faker) {
    return [
        'club_name' => $faker->unique()->state,
        'points' => $faker->randomDigit
    ];
});
